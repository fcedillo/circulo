#from query_modules.circulo import circulo_query
from query_modules.circulo_cache import circulo_query_cache


def query(RUC, Consulta, CIRCULO_API_KEY, CIRCULO_USERNAME, CIRCULO_PASSWORD, correlative, emission_date_str):

    consultas = Consulta.objects.filter(RUC__exact=RUC)

    if not consultas:
        consulta = Consulta()
        consulta.RUC = RUC
    else:
        consulta = consultas[0]

    consulta.emision_fecha = emission_date_str

    # circulo_query(consulta, CIRCULO_API_KEY, CIRCULO_USERNAME, CIRCULO_PASSWORD)
    circulo_query_cache(consulta, CIRCULO_API_KEY, CIRCULO_USERNAME, CIRCULO_PASSWORD)

    # consulta.save()
