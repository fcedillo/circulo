def get_emission_date_str(emission_date):
    import locale
    locale.setlocale(locale.LC_TIME, 'es_ES.UTF-8')
    emission_date_str = emission_date.strftime("%d de %B de %Y")
    return emission_date_str
