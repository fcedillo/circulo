import logging
import time
import requests
import requests_cache
import json


requests_cache.install_cache('db/circulo_cache', expire_after=2_592_000, allowable_methods=('POST',))


def initialize_logger():
    global logger
    format_colors = '%(asctime)s %(levelname)7s %(process)5d %(filename)20s %(lineno)3d %(message)s'
    logging.basicConfig(format=format_colors, level=logging.WARNING)
    logger = logging.getLogger()


def circulo_query_cache(consulta, CIRCULO_API_KEY, CIRCULO_USERNAME, CIRCULO_PASSWORD):

    global logger

    initialize_logger()

    start_time = time.time()
    
    RUC = consulta.RUC

    r = requests

    prototcol = 'https:'
    url = '//services.circulodecredito.com.pe'
    endpoint = '/v1/reporteCredito'
    uri = prototcol + url + endpoint

    CIRCULO_documentType = "10"

    payload = {
        "folio": "1",
        "tipoDocumento": CIRCULO_documentType,
        "numeroDocumento": RUC
    }

    json_text = json.dumps(payload)

    resp = requests.post(uri, data=json_text)

    if resp.from_cache:

        if resp.status_code != 200:
            if resp.status_code == 404:
                json_response = resp.json()
                errors = json_response['errores']
                for error in errors:
                    code = error['codigo']
                    message = error['mensaje']
                    logger.info(f'code: {code}. message: {message}')
            else:
                logger.info(resp.status_code)
                logger.info(resp.text)
                json_response = resp.json()
                for error in json_response['errores']:
                    code = error['codigo']
                    message = error['mensaje']
                    logger.info(f'code: {code}. message: {message}')
                    if code == '400.3':
                        consulta.document_number_is_valid = False
        else:

            consulta.CIRCULO_persona_encontrada = True

            json_response = resp.json()
            logger.info("status_code: " + str(resp.status_code))
            logger.info("Response body message: %s" % resp.text)
            logger.info("Response headers: %s" % resp.headers)
            logger.info("Total %s seconds execution" % (time.time() - start_time))
            
            logger.info('calificacion')
            calificacion24Meses = json_response['resumenCredito']['SBSUltimos24Meses']['calificacion24Meses']
            if not calificacion24Meses:
                logger.info("calificacion24Meses is empty")
            else:
                periods = {}
                for period in calificacion24Meses:
                    period_date = period['fechaPeriodo']
                    logger.info(f'period_date: {period_date}')
                    periods[period_date] = period
                if "202002" in periods.keys():
                    logger.info("has 2020 feb period")
                    deuda_tipos = [
                        'Normal',
                        'CPP',
                        'Deficiente',
                        'Dudoso',
                        'Perdida'
                    ]
                    SBS_clasificacion = {
                        'Normal': 'Normal',
                        'CPP': 'Con problemas potenciales',
                        'Deficiente': 'Deficiente',
                        'Dudoso': 'Dudoso',
                        'Perdida': 'Perdida'
                    }
                    deudaMontos = {}
                    for deuda_tipo in deuda_tipos:
                        deudaMontos[deuda_tipo] = periods['202002']['deuda' + deuda_tipo]
                    deudaTotal = sum(deudaMontos.values())
                    if deudaTotal != 0:
                        deudaPorcentajes = {}
                        greater_percentage = 0
                        worst_percentage_deuda_tipo = ''
                        for deuda_tipo in deuda_tipos:
                            percentage = deudaMontos[deuda_tipo] / deudaTotal * 100
                            setattr(consulta, 'SBS_clasificacion_deuda' + deuda_tipo + '_percentage', percentage)
                            deudaPorcentajes[deuda_tipo] = percentage
                            if percentage > 0:
                                worst_percentage_deuda_tipo = deuda_tipo
                        
                        if worst_percentage_deuda_tipo in deuda_tipos[2:]:
                            logger.info('in worst tipos')
                            if deudaPorcentajes['Normal'] + deudaPorcentajes['CPP'] >= 90:
                                consulta.clasificacion_SBS = SBS_clasificacion['CPP']
                            else:
                                consulta.clasificacion_SBS = SBS_clasificacion[worst_percentage_deuda_tipo]
                        else:
                            consulta.clasificacion_SBS = SBS_clasificacion[worst_percentage_deuda_tipo]

                        for k, v in deudaPorcentajes.items():
                            logger.info(f'{k}: {v:.2f} %')
                        logger.info(f'clasificacion_SBS: {consulta.clasificacion_SBS}')

            logger.info('protestos')
            protestosAceptante = json_response['otrasObligaciones']['protestosAceptante']
            if not protestosAceptante:
                logger.info("protestosAceptante is empty")
                protestos_sin_aclarar_CCL = 'Sin información'
                protestos_sin_aclarar_CCL_resultado = 'Aplica'
            else:
                logger.info('tiene protestos')
                protestos = []
                for protesto in protestosAceptante:
                    protestos.append(protesto['claveSituacion'])
                if 'NOACLA' in protestos:
                    consulta.protestos_sin_aclarar_CCL = 'No aclarado'
                    consulta.protestos_sin_aclarar_CCL_resultado = 'No aplica'
                else:
                    consulta.protestos_sin_aclarar_CCL = 'Aclarado'
                    consulta.protestos_sin_aclarar_CCL_resultado = 'Aplica'

    else:
        print('Not in cache')


