from django.db import models

from django.db import models


class Consulta(models.Model):

    # Datos de la empresa
    RUC = models.CharField(max_length=11)

    # Circulo de Crédito API
    CIRCULO_persona_encontrada = models.BooleanField(default=False, blank=True)
    document_number_is_valid = models.BooleanField(default=True, blank=True)


class Clasification(Consulta):
    class Meta:
        proxy = True

    def do_something(self):
        # ...
        pass


class Procesamiento(Consulta):
    class Meta:
        proxy = True

    def do_something(self):
        # ...
        pass


class Resultado(Consulta):
    class Meta:
        proxy = True

    def do_something(self):
        # ...
        pass

