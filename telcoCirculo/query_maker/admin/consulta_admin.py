from django.contrib import admin
from query_maker.models import Consulta
from .field_names import *


@admin.register(Consulta)
class ConsultaAdmin(admin.ModelAdmin):

    fieldsets = (
        ('Datos de la empresa', {
            'fields': (
                *datos_generales_fields,
            ),
        }),
    )

    readonly_fields = [
        *datos_generales_fields
    ]

    search_fields = ['RUC']