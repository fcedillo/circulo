from django.contrib import admin
from query_maker.models import Procesamiento
from .field_names import *


@admin.register(Procesamiento)
class ProcesamientoAdmin(admin.ModelAdmin):

    fieldsets = (
        ('Datos de la empresa', {
            'fields': (
                *datos_generales_fields,
            ),
        }),
        ('Datos de Procesamiento', {
            'fields': (
                procesamiento_fields
            ),
        }),
    )

    list_display = (
        'RUC',
    )

    list_display = *list_display, *procesamiento_fields

    list_filter = (
        'CIRCULO_persona_encontrada',
    )

    readonly_fields = [
        'RUC',
    ]
    
    readonly_fields = readonly_fields + procesamiento_fields

    search_fields = ['RUC']
